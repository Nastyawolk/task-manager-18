package ru.t1.volkova.tm.api.service;

import ru.t1.volkova.tm.api.repository.IUserRepository;
import ru.t1.volkova.tm.enumerated.Role;
import ru.t1.volkova.tm.model.User;

public interface IUserService extends IUserRepository {

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User removeById(String id);

    User removeByLogin(String login);

    User removeByEmail(String email);

    User setPassword(String id, String password);

    User updateUser(
            String id,
            String firstName,
            String lastName,
            String middleName
    );

}
