package ru.t1.volkova.tm.command.user;

import ru.t1.volkova.tm.model.User;
import ru.t1.volkova.tm.util.TerminalUtil;

public final class UserRegistryCommand extends AbstractUserCommand {

    private static final String DESCRIPTION = "Registry user";

    private static final String NAME = "user-registry";

    @Override
    public void execute() {
        System.out.println("[USER REGISTRY]");
        System.out.println("ENTER LOGIN");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD");
        final String password = TerminalUtil.nextLine();
        System.out.println("ENTER E-MAIL");
        final String email = TerminalUtil.nextLine();
        final User user = getAuthService().registry(login, password, email);
        showUser(user);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
