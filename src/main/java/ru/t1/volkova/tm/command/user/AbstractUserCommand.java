package ru.t1.volkova.tm.command.user;

import ru.t1.volkova.tm.api.service.IAuthService;
import ru.t1.volkova.tm.api.service.IUserService;
import ru.t1.volkova.tm.command.AbstractCommand;
import ru.t1.volkova.tm.exception.entity.UserNotFoundException;
import ru.t1.volkova.tm.model.User;

public abstract class AbstractUserCommand extends AbstractCommand {

    protected IAuthService getAuthService() {
        return serviceLocator.getAuthService();
    }

    protected IUserService getUserService() {
        return serviceLocator.getUserService();
    }

    @Override
    public String getArgument() {
        return null;
    }

    protected void showUser(final User user) {
        if (user == null) throw new UserNotFoundException();
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("E-MAIL: " + user.getEmail());
        System.out.println("ROLE: " + user.getRole().getDisplayName());
    }

}
