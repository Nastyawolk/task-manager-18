package ru.t1.volkova.tm.command.user;

import ru.t1.volkova.tm.util.TerminalUtil;

public final class UserUpdateProfileCommand extends AbstractUserCommand {

    private static final String DESCRIPTION = "Update profile of current user";

    private static final String NAME = "update-user-profile";

    @Override
    public void execute() {
        final String userId = getAuthService().getUserId();
        System.out.println("[USER UPDATE PROFILE]");
        System.out.println("LAST NAME:");
        final String lastName = TerminalUtil.nextLine();
        System.out.println("FIRST NAME:");
        final String firstName = TerminalUtil.nextLine();
        System.out.println("MIDDLE NAME:");
        final String middleName = TerminalUtil.nextLine();
        getUserService().updateUser(
                userId, firstName, lastName, middleName
        );
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
