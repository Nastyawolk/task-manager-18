package ru.t1.volkova.tm.command.task;

import ru.t1.volkova.tm.util.TerminalUtil;

public final class TaskUnbindFromProjectCommand extends AbstractTaskCommand {

    private static final String DESCRIPTION = "Unbind task from project.";

    private static final String NAME = "task-unbind-from-project";

    @Override
    public void execute() {
        System.out.println("[UNBIND TASK TO PROJECT]");
        System.out.println("[ENTER PROJECT ID]");
        final String projectID = TerminalUtil.nextLine();
        System.out.println("[ENTER TASK ID]");
        final String taskID = TerminalUtil.nextLine();
        projectTaskService().unbindTaskFromProject(projectID, taskID);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
